var fs = require("fs");

var diretorio = process.argv[2];
var extensao = process.argv[3];

fs.readdir(diretorio, function(err, arquivos) {
  var arquivosFiltrados = arquivos.filter(function(nomeArq) {
    return nomeArq.indexOf("." + extensao) !== -1;
  });
  
  console.log(arquivosFiltrados.join('\n'));
});
