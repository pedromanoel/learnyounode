var numeros = process.argv.slice(2).map(function(currVal, index, arr) {
	return Number(currVal);
});

var soma = numeros.reduce(function(prevVal, currVal, index, arr) {
	return prevVal + currVal;
}, 0);

console.log(soma);