var filteredls = require("./filteredls")

var diretorio = process.argv[2];
var extensao = process.argv[3];

filteredls(diretorio, extensao, function(err, data) {
  if (err) {
    console.log("ERROR");
  } else {
    console.log(data.join("\n"));
  }
})
