var http = require("http");

var url = process.argv[2];

http.get(url, function(response) {
  response.setEncoding("utf8");

  // se a única coisa que vou fazer é chamar o log, então
  // posso passar a funcão direto!
  response.on("data", console.log);
  response.on("error", console.error);
});
