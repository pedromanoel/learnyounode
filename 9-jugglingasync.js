var http = require("http");
var bl = require("bl");

var urls = process.argv.slice(2);

var conteudos = new Array();
var cont_completados = 0;

urls.forEach(function(url, index) {
  http.get(url, function(response) {
    response.setEncoding("utf8");

    response.on("data", function(data) {
      if (!conteudos[index]) {
        conteudos[index] = "";
      }

      conteudos[index] += data;
    });

    response.on("end", function() {
      cont_completados += 1;

      if (cont_completados === 3) {
        conteudos.forEach(function(data) {
          console.log(data);
        });
      }
    });
  });
});
