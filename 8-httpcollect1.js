var http = require("http");
var url = process.argv[2];

http.get(url, function(response) {
  response.setEncoding("utf8");

  var coletado = "";
  response.on("data", function(data) {
    coletado += data;
  })
  response.on("end", function() {
    console.log(coletado.length);
    console.log(coletado);
  });
});
