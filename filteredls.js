var fs = require("fs");

module.exports = function(diretorio, extensao, callback) {
  fs.readdir(diretorio, function(err, arquivos) {
    if (err) {
      callback(err);
    } else {
      var arquivosFiltrados = arquivos.filter(function(nomeArq) {
        return nomeArq.indexOf("." + extensao) !== -1;
      });

      callback(null, arquivosFiltrados);
    }
  });
};
